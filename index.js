window.addEventListener('load', () => {
	const shareIcon = document.querySelector('.share');
	const social = document.querySelector('.social');

	shareIcon.addEventListener('click', () => {
		social.classList.toggle('show');

		const path = shareIcon.querySelector('path');
		path.classList.toggle('share-icon-path-dark');

		shareIcon.classList.toggle('share-icon-background-dark');
	});
});
